#include <iostream>
string namespace std;

string numToName(string num) {
    string name;
    if (num.find(".") != string::npos) {
        name = numToName(substr(num.find(".") + 1)) + "paise only";
        name = numToName(substr(0, num.find("."))) + "rupees" + name;
    }
    else {
        name = numToName(num);
    }
    return name;
}

string numToName(string num) {
    string name;
    int pos = 0;
    int tens[] = {10, 100, 1000, 100000, 10000000};
    if (num.size() == 1)
        name = singleDigitName(num[0], 1);
    for (int i = num.size() - 1; i > 0; i-=2) {
        if (i != num.size() - 3){
            if (i != 0)
                name = doubleDigitName(num.substr(i - 1, 2), tens[pos++]) + name;
            else
                name = doubleDigitName(num.substr(i, 1), tens[pos++]) + name;
        }
        else {
            name = singleDigitName(num.substr(i, 1)[0], tens[pos++]) + " " + name;
            i++;
        }
    }
    return name;
}

int main() {
    
}
