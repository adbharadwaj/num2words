#include <iostream>
#include <map>
#include <sstream>
#include <string>

using namespace std;



string NumberToString(int pNumber)
{
    ostringstream oOStrStream;
    oOStrStream << pNumber;
    return oOStrStream.str();
}

/*Accepts digits between 1 and 9, returns their name.*/

string singleDigitName(char digit, int pos) {
    int inputDigit = digit - '0';

    if ((inputDigit == 0) && (pos == 100)) {
        return "";
    }

    string positionSuffix = "";
    if (pos == 100) {
        positionSuffix = " hundred";
    }

    string nameArray[10] = {"zero", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine"};

    for (int i = 0; i <= 9; i++) {
        if (inputDigit == i) {
            return (nameArray[i] + positionSuffix);
        }
    }

    return "ERROR. NO DIGIT MATCHING!";
}

string doubleDigitName (string number, int pos) {
    map<string, string> namedStrings;
    namedStrings["00"] = "";
    namedStrings["10"] = "ten";
    namedStrings["11"] = "eleven";
    namedStrings["12"] = "twelve";
    namedStrings["13"] = "thirteen";
    namedStrings["14"] = "fourteen";
    namedStrings["15"] = "fifteen";
    namedStrings["16"] = "sixteen";
    namedStrings["17"] = "seventeen";
    namedStrings["18"] = "eighteen";
    namedStrings["19"] = "nineteen";
    namedStrings["20"] = "twenty";
    namedStrings["30"] = "thirty";
    namedStrings["40"] = "forty";
    namedStrings["50"] = "fifty";
    namedStrings["60"] = "sixty";
    namedStrings["70"] = "seventy";
    namedStrings["80"] = "eighty";
    namedStrings["90"] = "ninety";

    map<int, string> posStrings;
    posStrings[1000] = "thousand";
    posStrings[100] = "hundred";
    posStrings[10] = "";
    posStrings[100000] = "lacs";
    posStrings[10000000] = "crores"; 

    string result;

    if(namedStrings.find(number) != namedStrings.end())
        result = namedStrings[number];
    else {
        int num = atoi(number.c_str());
        result = namedStrings[NumberToString(num - (num % 10))] + " " + singleDigitName(number[1], 1);
    }
    if (number != "00")
        result += " " + posStrings[pos];
    return result;
}

string numToName(string num) {
    string name;
    int pos = 0;
    int tens[] = {10, 100, 1000, 100000, 10000000};
    if (num.size() == 1)
        name = singleDigitName(num[0], 1);
    else {
        for (int i = num.size() - 1; i >= 0; i-=2) {
            if (i != num.size() - 3){
                if (i != 0)
                    name = doubleDigitName(num.substr(i - 1, 2), tens[pos++]) + " " + name;
                else
                    name = doubleDigitName("0" + num.substr(i, 1), tens[pos++]) + " " + name;
            }
            else {
                name = singleDigitName(num.substr(i, 1)[0], tens[pos++]) + " " + name;
                i++;
            }
        }
    }
    return name;
}


int main(int argc, char *argv[]) {
    cout<<numToName(argv[1]);
    return 0;
}
