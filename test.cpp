#include <iostream>
#include <fstream>
#include <sstream>
#include <string>

using namespace std;

string numToName(string num);
int test(char s[]);

int main(int argc, char *argv[]) {
    test(argv[1]);
    
   
}

int test(char str[]) {

    ifstream myReadFile;
    myReadFile.open(str);
    string line;

    if (myReadFile.is_open()) {
        while (!myReadFile.eof()) {
            getline(myReadFile, line);
            string input, expectedAns;
            for (int i = 0; i < line.length(); i++) {
                if (line[i] == '|') {
                input = line.substr(0, i);
                expectedAns = line.substr(i + 1);
                }
            }

            string ans = numToName(input);
            if (ans.compare(expectedAns) == 0)
                cout << "Test case correct " << endl;
            else
                cout << "Test case incorrect" <<endl;

           }

    }
    myReadFile.close();
    return 0;
}

string numToName(string num) {
    return "twenty";
} 
